const express = require("express");
const studentRouter = express.Router();
const studentModel = require("../models/student");

// get student data
studentRouter.get("/", (req, res) => {
  return new Promise((resolve, reject) => {
    studentModel
      .find()
      .then(data => {
        resolve(res.status(200).send(data));
      })
      .catch(error => {
        reject(res.status(400).send(error));
      });
  });
});

// create student
studentRouter.post("/add", (req, res) => {
  return new Promise((resolve, reject) => {
    let student = new studentModel();
    student.name = req.body.name;
    student.email = req.body.email;
    student.mobile = req.body.mobile;
    student.class = req.body.class;
    student.father = req.body.father;
    student.mother = req.body.mother;
    student.address = req.body.address;
    student
      .save()
      .then(data => {
        resolve(res.status(200).send(data));
      })
      .catch(error => {
        reject(res.status(400).send(error));
      });
  });
});

// update existing student
studentRouter.put("/update/:id", (req, res) => {
  return new Promise((resolve, reject) => {
    studentModel
      .findById(req.params.id)
      .then(student => {
        student.name = req.body.name;
        student.email = req.body.email;
        student.mobile = req.body.mobile;
        student.class = req.body.class;
        student.father = req.body.father;
        student.mother = req.body.mother;
        student.address = req.body.address;
        student
          .save()
          .then(data => {
            resolve(res.status(200).send(data));
          })
          .catch(error => {
            reject(res.status(400).send(error));
          });
      })
      .catch(error => {
        reject(res.send(error));
      });
  });
});

// delete student record
studentRouter.delete("/delete/:id", (req, res) => {
  return new Promise((resolve, reject) => {
      studentModel.findByIdAndDelete(req.params.id).then((data)=>{
        resolve(res.status(200).send(data))
      }).catch((error)=>{
        reject(res.status(400).send(error))
      })
  });
});

module.exports = studentRouter;
