const express = require('express');
const teacherRoute = express.Router();
const teacherModel = require("../models/teacher");
// get teacher data
teacherRoute.get('/', (req, res)=>{
    return new Promise((resolve, reject)=>{
        teacherModel.find().then((data)=>{
            resolve(res.status(200).send(data));
        }).catch((error)=>{
            reject(res.status(404).send(error))
        })
    })
});

// create teacher
teacherRoute.post('/add', (req, res)=>{
    return new Promise((resolve, reject)=>{
        let teacher = new teacherModel();
        teacher.name = req.body.name;
        teacher.email = req.body.email;
        teacher.mobile = req.body.mobile;
        teacher.save().then((data)=>{
            resolve(res.status(201).send(data));
        }).catch((error)=>{
            reject(res.status(404).send(error))
        })
    })
});

// update existing teacher
teacherRoute.put('/update/:id',(req, res)=>{
    return new Promise((resolve, reject)=>{
        let id = req.params.id;
        teacherModel.findById(id).then((teacher)=>{
            teacher.name = req.body.name;
            teacher.email = req.body.email;
            teacher.mobile = req.body.mobile;
            teacher.save().then(data =>{
                resolve(res.status(200).send(data))
            }).catch(err=>{
                reject(res.send(err));
            })
        }).catch((error)=>{
            reject(res.status(404).send(error));
        })
    })
});

// delete teacher record
teacherRoute.delete('/delete/:id',(req, res)=>{
    return new Promise((resolve, reject)=>{
        teacherModel.findByIdAndDelete(req.params.id).then((data)=>{
            resolve(res.status(200).send(data));
        }).catch((error)=>{
            reject(res.status(400).send(error));
        })
    })
});

module.exports = teacherRoute;