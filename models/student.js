const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const studentSchema = new Schema({
    name: String,
    email: String,
    mobile: String,
    class: String,
    father: String,
    mother: String,
    address: String
});

const Student = mongoose.model("student", studentSchema);
module.exports = Student;