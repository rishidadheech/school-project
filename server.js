const express = require('express');
const app = express();
require('dotenv').config();
const mongoose = require('mongoose');

mongoose.connect(process.env.DB_Connect);
const db = mongoose.connection; 
db.on('error', console.error.bind(console, 'connection error:')); 
db.once('open', function() {
  console.log("Connection Successful!");
});

const bodyParser = require("body-parser");
const studentRoute = require("./routes/student");
const teacherRoute = require("./routes/teacher");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/student', studentRoute);
app.use('/teacher', teacherRoute);


app.listen(process.env.PORT,()=>{
    console.log('port is '+process.env.PORT);
})